#!/usr/bin/env python3

import argparse
import json
import logging
import os
import subprocess
import tempfile
import time

import build_image_incr


class App:
    def __init__(self, state_dir, staging_dir, full, http_proxy, https_proxy):
        self.state_dir = state_dir
        self.staging_dir = staging_dir
        self.full = full
        self.http_proxy = http_proxy
        self.https_proxy = https_proxy

    def docker_build(
        self,
        dockerfile,
        context_dir="empty",
        target=None,
        tag=None,
        pull=False,
        build_args=None,
    ) -> str:
        """
        Builds an image using the specified `dockerfile`.

        Returns the output image id.
        """
        with tempfile.NamedTemporaryFile() as iidfile:
            cmd = [
                "docker",
                "build",
                "-f",
                dockerfile,
                "--iidfile",
                iidfile.name,
            ]
            if target:
                cmd += ["--target", target]
            if pull:
                cmd.append("--pull")
            if tag:
                cmd += ["--tag", tag]

            if self.http_proxy:
                cmd += ["--build-arg", f"http_proxy={self.http_proxy}"]
            if self.https_proxy:
                cmd += ["--build-arg", f"https_proxy={self.https_proxy}"]

            if build_args:
                for var, value in build_args.items():
                    if value:
                        cmd += ["--build-arg", f"{var}={value}"]

            cmd.append(context_dir)

            logging.info("Running %s", " ".join(cmd))
            subprocess.check_call(cmd)

            with open(iidfile.name) as i:
                image_id = i.readline().strip()

            return image_id

    def build_webserver_image(self, webserver_image_name):
        """
        Returns the output image name, which might be a previously-built image if
        nothing changed since the last build.
        """
        logging.info("Building webserver-image-base")

        self.docker_build(
            "webserver/Dockerfile.webserver-base-image",
            pull=True,
            tag="webserver-image-base",
        )

        tag = time.strftime("%Y-%m-%d-%H%M%S-webserver")
        fqin = f"{webserver_image_name}:{tag}"

        state_file = os.path.join(self.state_dir, "webserver-state.json")
        report_file = os.path.join(self.state_dir, "webserver-report.json")

        exclude = [
            "**/.git",
            "/wmf-config",
            "/tests",
            "/scap",
            "/src",
            "/multiversion",
            "/private",
            "/php-*",
        ]

        return build_image_incr.App(
            "webserver-image-base",
            "/srv/mediawiki-staging",
            "/srv/mediawiki",
            fqin,
            exclude=exclude,
            state_file=state_file,
            report_file=report_file,
            full=self.full,
            push=True,
        ).run()

    def build_mediawiki_images(
        self,
        mediawiki_versions: list,
        mv_image_name,
        mv_debug_image_name,
        force_version,
        mediawiki_image_extra_packages,
        mediawiki_extra_ca_cert,
    ):
        """
        Return the output image names of
        1) The multiversion image
        2) The multiversion debug image

        Returned image names may be names of previously-built images if nothing changed since the last build.
        """

        base = "single-version-base" if force_version else "multiversion-base"

        logging.info("Building %s", base)
        # FIXME: Rename directory to mediawiki-base
        self.docker_build(
            "multiversion-base/Dockerfile",
            context_dir="multiversion-base",
            tag=base,
            target=base,
            pull=True,
            build_args={
                "MV_BASE_PACKAGES": mediawiki_image_extra_packages,
                "MV_EXTRA_CA_CERT": mediawiki_extra_ca_cert,
            },
        )

        include = []
        exclude = [".git", "**/cache/l10n/upstream", "/scap"]

        # Add --include's for active branches, based on reading the staging directory's
        # wikiversions.json file. If FORCE_MW_VERSION is set, use it as
        # the only active branch, ignoring wikiversions.json.

        if force_version:
            include.append(f"/php-{force_version}")
        else:
            for version in mediawiki_versions:
                include.append(f"/php-{version}")

        # And exclude any other trees that may be lurking
        exclude.append("/php-*")

        tag = time.strftime("%Y-%m-%d-%H%M%S-publish")
        fqin = f"{mv_image_name}:{tag}"

        extra_commit_commands = []

        if force_version:
            extra_commit_commands.append(f"ENV FORCE_MW_VERSION={force_version}")

        state_file = os.path.join(self.state_dir, "mediawiki-mv-state.json")
        report_file = os.path.join(self.state_dir, "mediawiki-mv-report.json")

        logging.info(
            "Building %s with MediaWiki versions %s",
            mv_image_name,
            ", ".join(mediawiki_versions),
        )

        image = build_image_incr.App(
            base,
            self.staging_dir,
            "/srv/mediawiki",
            fqin,
            include=include,
            exclude=exclude,
            extra_commit_commands=extra_commit_commands,
            state_file=state_file,
            report_file=report_file,
            full=self.full,
            push=True,
        ).run()
        debug_image = self.build_mediawiki_debug_image(mv_debug_image_name, image, tag)

        return image, debug_image

    def build_mediawiki_debug_image(self, mv_debug_image_name, base, tag) -> str:
        """
        `base` is expected to be the multiversion image built by build_mediawiki_images().

        Returns the output image name, which might be a previously-built image if
        nothing changed since the last build.
        """
        logging.info("Building debug image")

        with tempfile.NamedTemporaryFile("w+") as dockerfile:
            dockerfile.write(
                f"""FROM {base}
    USER root
    RUN apt-get update && apt-get install -y php7.4-tideways php7.4-ldap
    USER 33
    """
            )
            dockerfile.flush()

            image_id = self.docker_build(dockerfile.name)

        state = {}
        state_file = os.path.join(self.state_dir, "mediawiki-mv-debug-state.json")
        if os.path.exists(state_file):
            with open(state_file) as f:
                state = json.load(f)

        if image_id == state.get("last_image_id"):
            mv_debug_image_name = state["last_image"]
            logging.info(
                "New debug image has the same image id as the prior, so using the old image %s",
                mv_debug_image_name,
            )
        else:
            mv_debug_image_name = f"{mv_debug_image_name}:{tag}"
            subprocess.check_call(["docker", "tag", image_id, mv_debug_image_name])
            state["last_image"] = mv_debug_image_name
            state["last_image_id"] = image_id
            write_json_file(state, state_file)

        logging.info("Pushing debug image to registry")
        subprocess.check_call(
            ["sudo", "/usr/local/bin/docker-pusher", "-q", mv_debug_image_name]
        )

        return mv_debug_image_name


def write_json_file(report, report_file):
    tmp_report_file = f"{report_file}.tmp"

    with open(tmp_report_file, "w") as f:
        json.dump(report, f, indent=4)
        f.write("\n")
    os.rename(tmp_report_file, report_file)


def main():
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s %(message)s", datefmt="%H:%M:%S"
    )

    ap = argparse.ArgumentParser()

    ap.add_argument("state_dir")

    ap.add_argument(
        "--staging-dir",
        help="The host path of the MediaWiki staging directory",
        default=os.environ.get("workdir_volume", "/srv/mediawiki-staging"),
    )
    ap.add_argument(
        "--mediawiki-versions",
        help="A comma-separated list of MediaWiki versions to include in the image",
    )
    ap.add_argument(
        "--full",
        help="Disable incremental build",
        action="store_true",
        default=os.environ.get("FORCE_FULL_BUILD") == "true",
    )
    ap.add_argument(
        "--multiversion-image-name",
        help="The name to use for the multiversion image, without tag",
        default=os.environ.get(
            "mv_image_name",
            "docker-registry.discovery.wmnet/restricted/mediawiki-multiversion",
        ),
    )
    ap.add_argument(
        "--multiversion-debug-image-name",
        help="The name to use for the multiversion debug image, without tag",
        default=os.environ.get(
            "mv_debug_image_name",
            "docker-registry.discovery.wmnet/restricted/mediawiki-multiversion-debug",
        ),
    )
    ap.add_argument(
        "--webserver-image-name",
        help="The name to use for the webserver image, without tag",
        default=os.environ.get(
            "webserver_image_name",
            "docker-registry.discovery.wmnet/restricted/mediawiki-webserver",
        ),
    )
    ap.add_argument(
        "--mediawiki-image-extra-packages",
        help="A space-separated list of extra packages to install in the mediawiki base image",
        default=os.environ.get("MV_BASE_PACKAGES"),
    )
    ap.add_argument(
        "--mediawiki-extra-ca-cert",
        help="A base64-encoded CA certificate to include in the mediawiki base image",
        default=os.environ.get("MV_EXTRA_CA_CERT"),
    )
    ap.add_argument(
        "--force-version",
        help="Create a single-version image using the specified version",
        default=os.environ.get("FORCE_MW_VERSION"),
    )
    ap.add_argument(
        "--http-proxy", help="HTTP proxy to use", default=os.environ.get("http_proxy")
    )
    ap.add_argument(
        "--https-proxy",
        help="HTTPS proxy to use",
        default=os.environ.get("https_proxy"),
    )
    args = ap.parse_args()

    state_dir = args.state_dir

    staging_dir = args.staging_dir
    if not staging_dir:
        raise SystemExit(
            "--staging-dir is required when 'workdir_volume' environment variable is not set"
        )
    if not staging_dir.startswith("/"):
        raise SystemExit("The staging directory must be an absolute path")

    app = App(
        state_dir,
        staging_dir,
        args.full,
        args.http_proxy,
        args.https_proxy,
    )

    logging.info("Building mediawiki images from %s", staging_dir)

    mw_mv_image, mw_mv_debug_image = app.build_mediawiki_images(
        args.mediawiki_versions.split(","),
        args.multiversion_image_name,
        args.multiversion_debug_image_name,
        args.force_version,
        args.mediawiki_image_extra_packages,
        args.mediawiki_extra_ca_cert,
    )
    webserver_image = app.build_webserver_image(args.webserver_image_name)

    report = {
        "mediawiki": {
            "multiversion-image": mw_mv_image,
            "multiversion-debug-image": mw_mv_debug_image,
        },
        "webserver": {
            "image": webserver_image,
        },
    }

    write_json_file(report, os.path.join(state_dir, "report.json"))


if __name__ == "__main__":
    main()
