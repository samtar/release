#!/usr/bin/env python3

import argparse
from phabricator import Phabricator
import re


def create_phab_task(task):
    return phab.maniphest.createtask(
        title=task['title'],
        description=task['description'],
        viewPolicy=task.get('viewPolicy', aclSec),
        editPolicy=task.get('editPolicy', aclSec),
        projectPHIDs=taskProjects,
    )


parser = argparse.ArgumentParser()
parser.add_argument(
    'versions',
    help='Versions to be used in titles/descriptions. e.g. 1.31.16/1.35.4/1.36.2'
)
parser.add_argument(
    'phabtoken',
    help='Phabricator api token to use to make the tasks. Starts api-'
)
args = parser.parse_args()

phab = Phabricator(host='https://phabricator.wikimedia.org/api/', token=args.phabtoken)

# TODO: Can we do more complex ACLs from maniphest task creation/editing? T376551
# TODO: Set subtype?
aclSec = 'PHID-PROJ-koo4qqdng27q7r65x3cw'

# projSecTeam = 'PHID-PROJ-pdw4jlcz543opbp2drhq'
projSec = 'PHID-PROJ-dwqfaiejpr656zc6hf6o'
projMWReleasing = 'PHID-PROJ-5p3mxnq5ejf4xs7cphgl'

taskProjects = [projSec, projMWReleasing]

version = args.versions

parentTask = create_phab_task({
    "title": "Release MediaWiki {0}".format(version),
    "description": """Previous release work: {{}}

Tracking for activities actually pertaining to making the release of MediaWiki {0}"""
    .format(version),
})

print("Parent task: https://phabricator.wikimedia.org/T{0}".format(parentTask['id']))

releaseNotes = ''
for (ver) in re.findall(r"1\.\d{2}", version):
    releaseNotes += "* https://www.mediawiki.org/wiki/Release_notes/{0}\n".format(ver)

subTasks = [
    {
        "title": "Tracking bug for MediaWiki {0}".format(version),
        "description": """Previous work: {{}}

Tracking bug for the next security release, {0}""".format(version),
    },
    {
        "title": "Obtain CVEs for {0} security releases".format(version),
        "description": "",
    },
    {
        "title": "Write and send pre-release announcements for MediaWiki {0}".format(version),
        # TODO: Populate release announcement task number
        "description": """Previous work: {}; release {}
This release announcement: {}""",
    },
    {
        "title": "Write and send release announcements for MediaWiki {0}".format(version),
        # TODO: Populate pre-release announcement task number
        "description": """Previous work: {}; pre-release {}
This pre-release announcement: {}""",
    },
    {
        "title": "Tag {0}".format(version),
        "description": "Create and push git tags for {0}".format(version),
    },
    {
        "title": "Update onwiki news and Module:Version for {0}".format(version),
        "description": "Update https://www.mediawiki.org/wiki/Template:MediaWiki_News"
                       " and https://www.mediawiki.org/wiki/Module:Version",
    },
    {
        "title": "Update onwiki release notes for {0}".format(version),
        "description":
            """The following mediawiki.org pages need updating from the RELEASE-NOTES files:
{0}
""".format(releaseNotes),
    },
    {
        "title": "Update Wikidata Q83 for {0}".format(version),
        "description": "Add new releases to https://www.wikidata.org/wiki/Q83",
    },
    {
        "title": "Update HISTORY in master after {0}".format(version),
        "description": ("Point release RELEASE-NOTES from {0} need copying to HISTORY "
                        "in the relevant places in master".format(version)),
    },
    {
        "title": ("Write and send supplementary release announcement for "
                  "extensions and skins with security patches ({0})".format(version)),
        "description": "Previous work: {}",
        # T307036
        "viewPolicy": "PHID-PLCY-rw34dnnz7s5d7njlpk5v",
        "editPolicy": "obj.subscriptions.subscribers",
        # T376553
        "subscribers": [
            "PHID-USER-agg5uweyh2mk6ywejjv2",  # mmartorana
            "PHID-USER-7kikgfw6jte3hait65nr",  # Mstyles
            "PHID-USER-6vzzsmi22zem6yttr6vp",  # Reedy
            "PHID-USER-dccizlq7dtc2zihfk7cd",  # sbassett
        ]
    },
]

subTaskPHIDS = []
for subTask in subTasks:
    subTaskPHID = create_phab_task(subTask)['phid']
    subTaskPHIDS.append(subTaskPHID)

    if 'subscribers' in subTask:
        phab.maniphest.edit(
            transactions=[{"type": "subscribers.add", "value": subTask['subscribers']}],
            objectIdentifier=subTaskPHID,
        )

phab.maniphest.edit(
    transactions=[{"type": "subtasks.add", "value": subTaskPHIDS}],
    objectIdentifier=parentTask['phid'],
)

print("Done!")
